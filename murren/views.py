from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from allauth.socialaccount.providers.vk.views import VKOAuth2Adapter
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.oauth2.client import OAuth2Client
from django.contrib.auth import get_user_model
from django.core.files.images import ImageFile
from django.db.models import Count
from django.http import HttpResponse
from rest_auth.registration.views import SocialLoginView
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from murr_notifications.models import SubscribeNotification
from murren.permissions import (
    IsAuthenticatedAndMurrenOrReadOnly,
    IsNotProfileOwner,
)
from murren.serializers import MurrenSerializer, VKOAuth2Serializer
from murren.services import make_crop_image, make_resize_image

Murren = get_user_model()


class MurrenPagination(PageNumberPagination):
    page_size = 30
    page_size_query_param = 'murren_len'
    max_page_size = 60


class MurrenViewSet(mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.ListModelMixin,
                    GenericViewSet):
    serializer_class = MurrenSerializer
    permission_classes = [IsAuthenticatedAndMurrenOrReadOnly]
    lookup_field = 'username'

    def get_queryset(self):
        queryset = Murren.objects.filter(is_active=True) \
            .annotate(subscribers_count=Count('subscribers')) \
            .annotate(likes_count=Count('murr_cards__liked_murrens', distinct=True)) \
            .annotate(dislikes_count=Count('murr_cards__disliked_murrens', distinct=True)) \
            .order_by('-date_joined')
        return queryset

    @action(detail=False, permission_classes=[IsAuthenticated])
    def profile(self, request):
        serializer = self.get_serializer(instance=request.user)
        return Response(serializer.data)

    @action(detail=False, permission_classes=[IsAuthenticated], methods=['POST'])
    def avatar_resize(self, request):
        file_avatar = request.FILES.get('avatar')
        b_image, error = make_resize_image(file_avatar, (500, 500))
        if error:
            return Response({"error": error}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return HttpResponse(b_image, content_type='image/jpeg')

    @action(detail=False, permission_classes=[IsAuthenticated], methods=['PATCH'])
    def avatar_update(self, request):
        file_avatar = request.FILES.get('avatar')
        crop_x = int(request.data.get('crop.x'))
        crop_y = int(request.data.get('crop.y'))
        crop_width = int(request.data.get('crop.width'))
        crop_height = int(request.data.get('crop.height'))

        b_image, error = make_crop_image(
            file_avatar, (crop_x, crop_y, crop_width, crop_height))

        if error:
            return Response({"error": error}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        murren_avatar = ImageFile(b_image, name=f"{request.user.pk}.jpeg")
        if request.user.murren_avatar != 'default_murren_avatar.png':
            request.user.murren_avatar.delete()
        request.user.murren_avatar = murren_avatar
        request.user.save()

        serializer = self.get_serializer(instance=request.user)

        return Response(serializer.data)

    @action(detail=True, permission_classes=[IsAuthenticated & IsNotProfileOwner], methods=['POST'])
    def subscribe(self, request, username=None):
        instance = self.get_object()
        subscriptions = request.user.subscriptions
        is_subscriber = subscriptions.filter(id=instance.id).exists()

        if is_subscriber is False:
            subscriptions.add(instance)
            if instance.show_subscription_notifications:
                notify, created = SubscribeNotification.objects.get_or_create(
                    to_murren=instance,
                    from_murren=request.user,
                )
                if not created:
                    notify.save(update_fields=['sent_time'])
        else:
            subscriptions.remove(instance)

        return Response({
            'is_subscriber': instance.in_subscribers(request.user),
            'subscribers_count': instance.subscribers.count()
        })

    @action(detail=False, permission_classes=[IsAuthenticated])
    def subscriptions(self, request):
        subscriptions = request.user.subscriptions \
            .annotate(subscribers_count=Count('subscribers')) \
            .order_by('subscribers_count')
        subscriptions = self.paginate_queryset(subscriptions)

        serializer = self.get_serializer(instance=subscriptions, many=True)

        return self.get_paginated_response(serializer.data)

    @action(detail=False, methods=['PATCH'], permission_classes=[IsAuthenticated])
    def switch_subscription_notifications(self, request):
        request.user.show_subscription_notifications = not request.user.show_subscription_notifications
        request.user.save(update_fields=['show_subscription_notifications'])
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=False, methods=['PATCH'], permission_classes=[IsAuthenticated])
    def switch_murr_card_notifications(self, request):
        request.user.show_murr_card_notifications = not request.user.show_murr_card_notifications
        request.user.save(update_fields=['show_murr_card_notifications'])
        return Response(status=status.HTTP_204_NO_CONTENT)


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter


class VkLogin(SocialLoginView):
    adapter_class = VKOAuth2Adapter
    serializer_class = VKOAuth2Serializer
    client_class = OAuth2Client


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter
