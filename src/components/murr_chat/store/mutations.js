import * as type from "./type.js";

export default {
  [type.CHAT_CLEAR]: (state) => {
    state.chatMessages = [];
    state.murrensOnline = [];
  },
  [type.CHAT_MESSAGES_SET]: (state, payload) => (state.chatMessages = payload),
  [type.CHAT_MURRENS_APPEND_ONLINE]: (state, data) =>
    (state.murrensOnline = [...state.murrensOnline, ...data]),
  [type.CHAT_MURREN_REMOVE_ONLINE]: (state, murren_name) =>
    (state.murrensOnline = state.murrensOnline.filter(
      (murren) => murren.name !== murren_name
    )),
  [type.CHAT_MESSAGE_APPEND]: (state, payload) =>
    (state.chatMessages = [...state.chatMessages, payload]),
  [type.CHAT_MESSAGES_PREPEND]: (state, payload) =>
    (state.chatMessages = [...payload, ...state.chatMessages]),
};
