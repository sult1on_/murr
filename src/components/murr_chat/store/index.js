import mutations from "./mutations.js";

export default {
  state: {
    chatMessages: [],
    murrensOnline: [],
  },
  getters: {
    chatMessages: (state) => state.chatMessages,
    murrensOnline: (state) => state.murrensOnline,
  },
  mutations,
};
