export const mutations = {
  changeShowRegisterForm_mutations(state) {
    state.showRegisterForm = !state.showRegisterForm;
  },
  changeShowLoginForm_mutations(state) {
    state.showLoginForm = !state.showLoginForm;
  },
  changeShowResetPasswordForm_mutations(state) {
    state.showResetPasswordForm = !state.showResetPasswordForm;
  },
};
