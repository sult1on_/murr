export const isJson = (str) => {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};

export const copyToBuffer = (string) => {
  const input = document.createElement("input");
  input.value = string;
  document.body.appendChild(input);
  input.select();
  document.execCommand("copy");
  document.body.removeChild(input);
};

export const handlerErrorAxios = (error) => {
  let obj = { success: false };

  if (error.response && error.response.data && error.response.data.detail) {
    obj.message = error.response.data.detail;
  } else {
    obj.message = "Что-то пошло не по плану 😮";
  }

  return obj;
};
