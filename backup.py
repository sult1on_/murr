import os
import yadisk
import shutil
import time


os.system('/usr/local/bin/python3 /home/murrengan/manage.py dumpdata -a -o /home/murrengan/database.json')

time.sleep(300)

y = yadisk.YaDisk(token=os.getenv('YANDEX_DISK_TOKEN'))

date = time.strftime("%Y-%m-%d", time.localtime())

if not y.is_dir('db_backup'):
    y.mkdir('db_backup')

if not y.is_dir(date):
    y.mkdir(f'db_backup/{date}')

shutil.make_archive(f'/home/murrengan/media', 'zip', '/home/murrengan/media')

y.upload(path_or_file=f'/home/murrengan/media.zip', dst_path=f'db_backup/{date}/media.zip', timeout=(15, 250))
y.upload(path_or_file=f'/home/murrengan/database.json', dst_path=f'db_backup/{date}/database.json', timeout=(15, 250))

os.remove(f'/home/murrengan/media.zip')
os.remove(f'/home/murrengan/database.json')
