from django.apps import AppConfig


class MurrNotificationsConfig(AppConfig):
    name = 'murr_notifications'

    def ready(self):
        import murr_notifications.signals
