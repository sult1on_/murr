from rest_framework.routers import DefaultRouter

from murr_notifications.views import NotificationsViewSet

router = DefaultRouter()
router.register('', NotificationsViewSet, basename='notifications')

urlpatterns = [
]

urlpatterns += router.urls
