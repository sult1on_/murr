from channels.routing import ProtocolTypeRouter, URLRouter
from channels.sessions import SessionMiddlewareStack

from murr_back.middleware import SocketTokenAuthMiddleware
from murr_websocket.routing import websocket_urls

application = ProtocolTypeRouter({
    'websocket': SessionMiddlewareStack(SocketTokenAuthMiddleware(URLRouter(websocket_urls)))
})
