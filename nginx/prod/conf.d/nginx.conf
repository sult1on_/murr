upstream django {
    server django:8000;
}
upstream daphne {
    server daphne:4154;
}

server {
    listen 80 default_server;
    listen [::]:80 default_server;
    server_name murrengan.ru www.murrengan.ru;
    return 301 https://murrengan.ru$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name www.murrengan.ru;
    ssl_certificate /etc/nginx/conf.d/origin_ca_rsa_root.pem;
    ssl_certificate_key /etc/nginx/conf.d/private_origin_ca_ecc_root.pem;
    return 301 https://murrengan.ru$request_uri;
}

server {
    client_max_body_size 10m;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name murrengan.ru;

    ssl_certificate /etc/nginx/conf.d/origin_ca_rsa_root.pem;
    ssl_certificate_key /etc/nginx/conf.d/private_origin_ca_ecc_root.pem;

    location /kibana {
        proxy_set_header        Host $host;
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto $scheme;
        proxy_set_header        Connection "Keep-Alive";
        proxy_set_header        Proxy-Connection "Keep-Alive";

        proxy_pass              http://kibana:5601/;
        proxy_read_timeout      90;
        proxy_redirect          http://kibana:5106 https://murrengan.ru/kibana;
        rewrite                 /kibana/(.*)$ /$1 break;
        proxy_buffering         off;

        auth_basic "Restricted Content";
        auth_basic_user_file /etc/nginx/kibana_cred/htpasswd.users;

    }

    location / {
      root /home/murrengan/murr/dist;
      index index.html;
      try_files $uri $uri/ /index.html;
    }


    location /ws/ {
        proxy_pass http://daphne;

        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host $server_name;
    }

    location /admin_panel_secure_url/ {
        proxy_pass http://django;
        limit_req zone=murr_back burst=40;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
        proxy_set_header Host $host;
        proxy_redirect off;
    }
    location /api/ {
        proxy_pass http://django;
        limit_req zone=murr_back burst=40;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_redirect off;
    }
    location /auth/ {
        proxy_pass http://django;
        limit_req zone=murr_back burst=40;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
        proxy_set_header Host $host;
        proxy_redirect off;
    }
    location /static/ {
        alias /home/murrengan/staticfiles/;
    }
    location /media/ {
        alias /home/murrengan/media/;
    }

}
