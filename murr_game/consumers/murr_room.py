import json
import math
import os
from datetime import datetime

import elasticsearch
import redis
from channels.db import database_sync_to_async
from django.conf import settings

from murr_game import mpv
from murr_game.engine.skill_impact.skill import prepare_chosen_skill
from murr_game.engine.turn_evaluating import TurnEvaluating
from murr_game.models import MurrGameRoom, Player
from murr_websocket.consumers.base import BaseMurrWebSocketConsumer
from murren.serializers import MurrenSerializer

murr_redis = redis.Redis(host=os.getenv("REDIS_HOST", "127.0.0.1"), port=os.getenv("REDIS_PORT", 6379), db=0)

es = elasticsearch.Elasticsearch('http://elasticsearch:9200')


def murr_logger(index, data: dict):
    murr_logger_on_elastic = os.getenv("USE_ELASTIC", "")
    if murr_logger_on_elastic:
        es.index(index=index, body={'data': data, 'timestamp': datetime.now()})
    else:
        print(index, data)


class MurrGameRoomConsumer(BaseMurrWebSocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.murr_game_room_instance = None

    async def connect(self):
        if 'user' not in self.scope:
            await self._send_message({'detail': 'Authorization failed'})
            await self.close(code=1000)
            return
        self.scope["murren"] = await self.get_murren(self.scope["user"])
        self.scope['room_name'] = self.scope['url_route']['kwargs']['room_name']
        murr_game_room_instance = await self.get_murr_game_room_instance()
        if not murr_game_room_instance:
            await self._trow_error({'detail': 'murr_game_room_instance not found'})
            await self.close()
            return
        self.murr_game_room_instance = murr_game_room_instance
        await self.channel_layer.group_add(self.scope['room_name'], self.channel_name)

        await self.accept()

        data = {
            'first_group': list(murr_game_room_instance.first_group_in_murr_game_room.all().values()),
            'second_group': list(murr_game_room_instance.second_group_in_murr_game_room.all().values()),
        }
        data = await self.prepare_battle_scene(data)

        murr_logger('murr_game_prepared', {'body': data})

        data['murr_game_room_instance'] = str(murr_game_room_instance)
        murr_redis.set(str(murr_game_room_instance), json.dumps(data))
        await self._send_message(data, gan='murrGameRoomPrepared')

    @database_sync_to_async
    def get_murr_game_room_instance(self):
        murr_game_room_instance = MurrGameRoom.objects.filter(room_name=self.scope['room_name']).first()
        return murr_game_room_instance

    @database_sync_to_async
    def prepare_battle_scene(self, data):

        turn_list = []

        for group_index, group in enumerate(data):
            for i, murren in enumerate(data[group]):
                murren_player = Player.objects.filter(murren_player_id=murren['murren_player_id']).first()
                if murren_player:
                    skills = []
                    murren_avatar = MurrenSerializer.get_avatar(None, murren_player.murren_player)

                    murren_player_data = {
                        'murren_id': murren_player.murren_player.pk,
                        'murren_name': murren_player.murren_player.username,
                        'murren_avatar': murren_avatar,
                        'murren_group': group,

                        'stats': {
                            'intelligence': murren_player.intelligence,
                            'vitality': murren_player.vitality,
                            'strength': murren_player.strength,
                            'dexterity': murren_player.dexterity,
                        },
                        'evasion': mpv["BASE_EVASION"] + math.ceil(
                            murren_player.dexterity / mpv["EVASION_MULTIPLY"]) * mpv["EVASION_INCREASE"],
                        'armor': mpv["ARMOR"] + math.ceil(
                            murren_player.vitality / mpv["ARMOR_MULTIPLY"]) * mpv["ARMOR_INCREASE"],
                        'critical_chance': mpv["CRITICAL_CHANCE"] + math.ceil(
                            murren_player.dexterity / mpv["CRITICAL_CHANCE_MULTIPLY"]),
                        'critical_damage': mpv["CRITICAL_DAMAGE"] + math.ceil(
                            murren_player.strength / mpv["CRITICAL_DAMAGE_MULTIPLY"]) * 10,

                        'hp': mpv["BASE_HP"] + math.ceil(murren_player.vitality * mpv["VITALITY_HP_MULTIPLY"]),

                        'max_hp': mpv["BASE_HP"] + math.ceil(murren_player.vitality * mpv["VITALITY_HP_MULTIPLY"]),
                        'turn_points': mpv['BASE_TURN_POINTS'],
                        'initiative': murren_player.intelligence,
                        'skills': skills
                    }

                    for skill in murren_player.skills.all():
                        prepared_skill = prepare_chosen_skill(murren_player_data, None, skill.name)
                        murren_player_data['skills'].append({
                            'id': skill.id,
                            'name': skill.name,
                            'cost': skill.cost,
                            'reload': skill.reload,
                            'sensitive_to_evasion': skill.sensitive_to_evasion,
                            'animation_cooldown': skill.animation_cooldown,
                            'type': skill.type.value,
                            'description': skill.description,
                            'logic': skill.logic,
                            'self_auto_use': skill.self_auto_use,
                            'gif': f'{settings.BACKEND_URL}{skill.gif.url}',
                            'icon': f'{settings.BACKEND_URL}{skill.icon.url}',
                            'init_damage': prepared_skill.init_damage(skill.type.value, mpv,
                                                                      murren_player_data['stats'],
                                                                      skill.base_damage),
                        })

                    data[group][i]['murren_player_data'] = murren_player_data
                    turn_list.append({'murren_name': murren_player.murren_player.username,
                                      'murren_group': group,
                                      'murren_avatar': murren_avatar,
                                      'initiative': murren_player.intelligence})

        # clear data from trash
        for group_index, group in enumerate(data):
            for i, murren in enumerate(data[group]):
                murren.pop('group_len')
                if murren['murren_player_id'] is None:
                    data[group].pop(i)

        data['turn_list'] = sorted(turn_list, key=lambda m: m['initiative'], reverse=True)
        return data

    async def gan__murren_leave_battle_room(self, gan):
        murren_leaver = gan['data'].get('murren_name')
        data = {
            'murren_leaver_name': murren_leaver
        }
        return await self._group_send(data, gan=gan['gan'], group=self.scope['room_name'])

    async def gan__turn_eval(self, gan):
        turn = TurnEvaluating(self._group_send, self.scope, gan, murr_redis, murr_logger)
        await turn.run()
        del turn
