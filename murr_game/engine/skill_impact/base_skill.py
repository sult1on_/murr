import math
from random import randrange

from murr_game.engine.init_damage_by_type import init_damage_by_type


class BaseSkill:
    def __init__(self, now_turn_murren_data, room_data):
        self.now_turn_murren_data = now_turn_murren_data
        self.room_data = room_data
        self.init_damage_by_type = init_damage_by_type

    def change_temporary_skill_data(self, new_values):
        """
        new_values = {
            'css_animation': 'successful_evasion',
            'gif': '',
            'damage': 0
        }
        """
        for k, v in new_values.items():
            self.room_data['temporary_skill_data'][k] = v

    def init_damage(self, damage_type, mpv, murren_stats, skill_base_damage):
        pass

    async def damage(self, init_damage):
        return await self.get_damage_dice(init_damage['min_damage'], init_damage['max_damage'])

    async def armor(self, armor):
        return armor

    async def evasion(self, evasion):
        return evasion

    @classmethod
    async def get_damage_dice(cls, min_damage, max_damage):
        return randrange(min_damage, max_damage + 1)

    async def critical_chance(self, damage, critical_chance_multiply=None):
        critical_chance = self.now_turn_murren_data['critical_chance']
        critical_damage = self.now_turn_murren_data['critical_damage']
        self.change_temporary_skill_data({'css_animation': 'dice'})

        if critical_chance_multiply:
            critical_chance += critical_chance_multiply

        successful_critical_chance_values = [x for x in range(1, critical_chance + 1)]
        dice = randrange(1, 101)

        if dice in successful_critical_chance_values:
            damage = math.ceil(damage * critical_damage / 100)
            self.change_temporary_skill_data({'css_animation': 'throw'})

        return damage
