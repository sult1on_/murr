from django.db.models import Count
from rest_framework import status
from rest_framework.decorators import action

from rest_framework.exceptions import ValidationError
from rest_framework.generics import ListAPIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response

from murr_game import mpv
from murr_game.engine.utils.helpers import check_min_skill_value
from murr_game.models import Player
from murr_game.permissions import IsAuthenticatedAndMurrenPlayerOrReadOnly
from murr_game.serializers import PlayerSerializer, PlayerLeaderBoardSerializer


class PlayerViewSet(ModelViewSet):
    serializer_class = PlayerSerializer
    lookup_field = 'murren_player__username'
    permission_classes = [IsAuthenticatedAndMurrenPlayerOrReadOnly]

    def get_queryset(self):
        queryset = Player.objects.select_related('murren_player')
        return queryset

    def update(self, request, *args, **kwargs):
        intelligence = request.data['intelligence']
        strength = request.data['strength']
        dexterity = request.data['dexterity']
        vitality = request.data['vitality']

        if not all(map(check_min_skill_value, (intelligence, strength, dexterity, vitality))):
            raise ValidationError({'error': 'У каждого атрибута должно быть минимум 5 очков'})

        instance = self.get_object()
        current_stats = instance.intelligence + instance.strength + instance.dexterity + instance.vitality
        max_sum_stats = current_stats + instance.available_points
        new_stats = intelligence + strength + dexterity + vitality

        if new_stats > max_sum_stats:
            raise ValidationError({'error': 'Превышен лимит доступных очков'})

        available_points_changes = abs(new_stats - current_stats - instance.available_points)
        instance.available_points = available_points_changes

        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(detail=True, permission_classes=[IsAuthenticatedAndMurrenPlayerOrReadOnly], methods=['POST'])
    def reset_stats(self, *args, **kwargs):
        instance = self.get_object()
        instance.intelligence = 5
        instance.strength = 5
        instance.dexterity = 5
        instance.vitality = 5
        instance.available_points = 28
        instance.save()
        return Response(PlayerSerializer(instance).data, status=status.HTTP_200_OK)


class PlayerLeaderBoardViewSet(ListAPIView):
    serializer_class = PlayerLeaderBoardSerializer
    permission_classes = [IsAuthenticatedAndMurrenPlayerOrReadOnly]

    def get_queryset(self):
        return Player.objects.select_related('murren_player')\
            .annotate(Count('won_games', distinct=True),
                      Count('games', distinct=True))\
            .order_by('-won_games__count', '-games__count')
